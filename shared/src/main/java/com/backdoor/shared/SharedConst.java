package com.backdoor.shared;

/**
 * Copyright 2015 Nazar Suhovich
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class SharedConst {
    public static final String KEY_TASK = "key_task";
    public static final String KEY_TYPE = "key_type";

    public static final String REQUEST_KEY = "request_key";

    public static final String WEAR_REMINDER = "/wear/reminder/data";
    public static final String WEAR_DISTANCE = "/wear/distance/data";

    public static final String PHONE_REMINDER = "/phone/reminder/button";

    public static final int KEYCODE_CALL = 11;
    public static final int KEYCODE_MESSAGE = 10;
    public static final int KEYCODE_OK = 12;
    public static final int KEYCODE_FAVOURITE = 13;
}
