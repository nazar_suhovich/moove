package com.backdoor.moove.core.consts;

public class Configs {

    /**
     * Max volume.
     */
    public static final int MAX_VOLUME = 25;

    /**
     * Application CardView elevation value.
     */
    public static final float CARD_ELEVATION = 5f;
}
