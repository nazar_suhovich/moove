package com.backdoor.moove.core.consts;

public class Constants {

    public static final String LOG_TAG = "Moove";
    public static final String ITEM_ID_INTENT = "itemId";

    // Contact List Constants
    public final static int REQUEST_CODE_CONTACTS = 101;
    public final static int REQUEST_CODE_SELECTED_MELODY = 115;
    public final static int REQUEST_CODE_SELECTED_RADIUS = 116;
    public final static int REQUEST_CODE_LED_COLOR = 118;

    public final static String SELECTED_CONTACT_NUMBER = "selected_number";
    public final static String SELECTED_MELODY = "selected_melody";
    public final static String SELECTED_RADIUS = "selected_radius";
    public final static String SELECTED_LED_COLOR = "selected_led_color";

    public static final String EDIT_ID = "edit_id";

    public static final String TYPE_LOCATION = "location";
    public static final String TYPE_LOCATION_OUT = "out_location";
    public static final String TYPE_LOCATION_OUT_CALL = "out_location_call";
    public static final String TYPE_LOCATION_OUT_MESSAGE = "out_location_message";
    public static final String TYPE_LOCATION_CALL = "location_call";
    public static final String TYPE_LOCATION_MESSAGE = "location_message";
    public static final String TYPE_MESSAGE = "message";
    public static final String TYPE_CALL = "call";

    public static final int NOT_LOCKED = 5;
    public static final int LOCKED = 6;

    public static final int SHOWN = 3;
    public static final int NOT_SHOWN = 4;

    public static final int ENABLE = 1;
    public static final int DISABLE = 2;

    public static final String FILE_PICKED = "file_selected";
    public static final String DEFAULT = "default";
    public static final String NONE = "none";
    public static final int ACTION_REQUEST_GALLERY = 130;
    public static final String SELECTED_CONTACT_ARRAY = "selected_contacts";
    public static final String SELECTED_CONTACT_NAME = "selected_contact_name";
    public static final String SELECTED_VOLUME = "selected_volume";
    public static final int REQUEST_CODE_VOLUME = 120;
}
