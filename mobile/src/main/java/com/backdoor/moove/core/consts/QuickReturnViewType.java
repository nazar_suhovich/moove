package com.backdoor.moove.core.consts;

public enum QuickReturnViewType {
    HEADER,
    FOOTER,
    BOTH,
    GOOGLE_PLUS,
    TWITTER
}
