package com.backdoor.moove.core.consts;

/**
 * Copyright 2016 Nazar Suhovich
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Prefs {

    public static final String MAP_TYPE = "map_type";
    public static final String LOCATION_RADIUS = "radius";
    public static final String TRACKING_NOTIFICATION = "tracking_notification";
    public static final String VIBRATION_STATUS = "vibration_status";
    public static final String SILENT_SOUND = "sound_status";
    public static final String WAKE_STATUS = "wake_status";
    public static final String INFINITE_SOUND = "infinite_sound";
    public static final String SILENT_SMS = "silent_sms";
    public static final String SILENT_CALL = "silent_call";
    public static final String LED_STATUS = "led_status";
    public static final String LED_COLOR = "led_color";
    public static final String MARKER_STYLE = "marker_style";
    public static final String INFINITE_VIBRATION = "infinite_vibration";
    public static final String WEAR_NOTIFICATION = "wear_notification";
    public static final String TRACK_TIME = "tracking_time";
    public static final String TRACK_DISTANCE = "tracking_distance";
    public static final String UNLOCK_DEVICE = "unlock_device";
    public static final String VOLUME = "reminder_volume";
    public static final String TTS = "tts_enabled";
    public static final String TTS_LOCALE = "tts_locale";
    public final static String CUSTOM_SOUND = "custom_sound";
    public final static String CUSTOM_SOUND_FILE = "sound_file";
    public final static String REMINDER_IMAGE = "reminder_image";
    public final static String REMINDER_IMAGE_BLUR = "reminder_image_blur";
    public static final String IS_24_TIME_FORMAT = "24_hour";
    public static final String LAST_USED_REMINDER = "last_reminder";
    public static final String RATE_SHOW = "rate_shown";
    public static final String APP_RUNS_COUNT = "app_runs";
    public static final String FIRST_LOAD = "first_loading";
    public static final String WEAR_SERVICE = "wear_service";
    public static final String PLACES_AUTO = "places_auto";
}
